#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

static  const  char *dirpath = "/home/james/Documents";

void encrypt_vcipher(char *string){
    // if(strcmp(path_file, ".") == 0 || strcmp(path_file, "..")== 0)return;
    
    char key[] = "INNUGANTENG";
    
    for(int i=0; i<strlen(string); i++){
        if('A'<=string[i] && string[i] <='Z')
            string[i] = ((string[i] - 'A' + (key[i%strlen(key)]) - 'A') %26) + 'A';
        else if('a'<=string[i] && string[i] <='z')
            string[i] = ((string[i] - 'a' + (key[i%strlen(key)]) - 'a') %26) + 'a';
    }
}

void decrypt_vcipher(char *string){
    // if(strcmp(path_file, ".") == 0 || strcmp(path_file, "..")== 0)return;

    char key[] = "INNUGANTENG";

    for (int i = 0; string[i]; i++) 
    {
        if ('A' <= string[i] && string[i] <= 'Z') 
            string[i] = ((string[i]-'A'-(key[i%((strlen(key)))]-'A')+26)%26)+'A';
        else if ('a' <= string[i] && string[i] <= 'z') 
            string[i] = ((string[i]-'a'-(key[i%((strlen(key)-1))]-'A')+26)%26)+'a';
    }
}


static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    char dec_path[1000];
    strcpy(dec_path, path); 
    decrypt_vcipher(dec_path);
    sprintf(fpath, "%s%s",dirpath,dec_path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

void checkDir(char* directoryname)
{
    char arr[]= directoryname;
    char checker[] = "nam_do-saq_";

    int arrLen = sizeof checker / sizeof checker[0];
    int isElementPresent = 0;

    //mengecek direktori     
    for (int i = 0; i < arrLen; i++) {
        if (arr[i] == checker[i]) {
            isElementPresent++;
            if (isElementPresent == arrLen - 1)
            {
                //fungsi direktori spesial
                break;
            }
            else
            {
                continue;
            }
            break;
        }
    }
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    // .readdir = xmp_readdir,
    // .read = xmp_read,
    // .rename = xmp_rename,
    // .mkdir = xmp_mkdir,
};

int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

