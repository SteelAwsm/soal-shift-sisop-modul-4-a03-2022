# soal-shift-sisop-modul-4-A03-2022



# Laporan Penjelasan dan Penyelesaian Soal

## Kelompok A03

| Nama          | NRP           |
|---------------|---------------|
|James Silaban                  | 5025201169|
|Hemakesha Ramadhani Heriqbaldi | 5025201209|
|Wina Tungmiharja               | 5025201242| 

# Soal 1

# Soal 2

## Membuat fungsi encrypt dan decrypt
```
void encrypt_vcipher(char *string){
    // if(strcmp(path_file, ".") == 0 || strcmp(path_file, "..")== 0)return;
    
    char key[] = "INNUGANTENG";
    
    for(int i=0; i<strlen(string); i++){
        if('A'<=string[i] && string[i] <='Z')
            string[i] = ((string[i] - 'A' + (key[i%strlen(key)]) - 'A') %26) + 'A';
        else if('a'<=string[i] && string[i] <='z')
            string[i] = ((string[i] - 'a' + (key[i%strlen(key)]) - 'a') %26) + 'a';
    }
}

void decrypt_vcipher(char *string){
    // if(strcmp(path_file, ".") == 0 || strcmp(path_file, "..")== 0)return;

    char key[] = "INNUGANTENG";

    for (int i = 0; string[i]; i++) 
    {
        if ('A' <= string[i] && string[i] <= 'Z') 
            string[i] = ((string[i]-'A'-(key[i%((strlen(key)))]-'A')+26)%26)+'A';
        else if ('a' <= string[i] && string[i] <= 'z') 
            string[i] = ((string[i]-'a'-(key[i%((strlen(key)-1))]-'A')+26)%26)+'a';
    }
}
```
Fungsi `encrypt_vcipher()` digunakan untuk mengencode dengan algoritma Vigenere Cipher dengan key `INNUGANTENG`. Sebaliknya, fungsi `decrypt_vcipher()` digunakan untuk mendecode bila nama folder tidak lagi memenuhi syarat dengan algoritma dan key yang sama.

## Mendapatkan attribut
```
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    char dec_path[1000];
    strcpy(dec_path, path); 
    decrypt_vcipher(dec_path);
    sprintf(fpath, "%s%s",dirpath,dec_path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}
```
Fungsi `xmp_getattr()` digunakan untuk mendapatkan atribut dari sebuah file

## Fungsi Utama
```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .mkdir = xmp_mkdir,
};

int  main(int  argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

```
Semua fungsi akan digabung didalam sebuah struct, yaitu `xmp_oper`

# Soal 3
```
    char arr[]= $directoryname;
    char checker[] = "nam_do-saq_";

    int arrLen = sizeof checker / sizeof checker[0];
    int isElementPresent = 0;

    //mengecek direktori     
    for (int i = 0; i < arrLen; i++) {
        if (arr[i] == checker[i]) {
            isElementPresent++;
            if (isElementPresent == arrLen - 1)
            {
                //fungsi direktori spesial
                break;
            }
            else
            {
                continue;
            }
            break;
        }
    }
```
- penjelasan: <br />
looping digunakan untuk mengecek jika suatu direktori memiliki elemen `nam_do-saq_` di awal nama direktori tersbut, dengan cara membandingkan elemen di dalam `arr[i]` yang berisi nilai dari variabel `directoryname` dengan elemen di dalam `checker[]`.
